<?php

require_once './classe/Member/Member.php';
require_once './classe/Task/Task.php';

$pierre = new Member('Pierre', 'Poljak', 'pp@pp.com', 'dev');
$task1 = new Task('first task', new DateTime(), 60, 'creation');
$task1->setMember($pierre);
