<?php

class Member
{
    private string $firstname;
    private string $lastname;
    private string $email;
    private string $role;

    public function __construct(string $firstname, string $lastname, string $email, string $role)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->role = $role;
    }

    public function getRole(): string
    {
        return $this->role;
    }
}
