<?php

require_once './classe/Member/Member.php';

class Task
{
    private string $label;
    private DateTime $date;
    private int $duration;
    
    private ?Member $member;

    public function __construct(string $label, DateTime $date, int $duration)
    {
        $this->label = $label;
        $this->date = $date;
        $this->duration = $duration;
        
    }

    public function setMember(?Member $member): self
    {
        $rel = [
            'dev' => ['dev', 'lead-dev'],
            'test' => ['dev', 'lead-dev', 'test', 'pm'],
            'creation' => ['da', 'webdesigner'],
        ];

        if (!isset($rel[$this->type]) || !in_array($member->getRole(), $rel[$this->type])) {
            throw new Exception("cannot assign member to task", 1);
        }

        $this->member = $member;

        return $this;
    }
}
